The code is designed to make a simulation with the same wheel inputs as in the MATLAB. 
The goal is to compare two data from gazebo and matlab in order to gain a deeperu nderstanding on robot kinematics.

To run the simulation:

1. Run: roslaunch robot_kinematics jackal_world.launch

2. Go to robot_kinematics/robot_kinematics/scripts

3. Open robot_kinematics_ver_3.py with the editor you prefer

4. There are three shapes provided. Uncomment the one you pick and save the file.

5. Run: rosrun robot_kinematics robot_kinematics_ver_3.py
