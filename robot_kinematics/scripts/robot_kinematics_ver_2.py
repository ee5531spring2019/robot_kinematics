#!/usr/bin/env python

import rospy 
import math
import time
import csv
import numpy as np
import scipy.integrate as integrate
from std_msgs.msg import String  
from geometry_msgs.msg import Twist

def kine(phi_1_dot, phi_2_dot, theta):
    r = 0.1
    l = 0.215

    R = np.array([[math.cos(theta), -math.sin(theta), 0],
               [math.sin(theta), math.cos(theta), 0],
               [0, 0, 1]])

    s = np.array([(r*phi_1_dot)/2 + (r*phi_2_dot)/2, 0,
               (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)])
               
    I = R.dot(s)
               
    x_dot = I[0]
    y_dot = I[1]
    theta_dot = I[2]
    print I
    return x_dot, y_dot, theta_dot
    
def wheel(time):
    # left = 2
    # right = 1
 
    # Square
    if time <= 2:
        phi_1_dot = 2
        phi_2_dot = 2
    elif time > 2 and time <= 2.6:
        phi_1_dot = 5.64
        phi_2_dot = -5.64
    elif time > 2.6 and time <= 4.7:
        phi_1_dot = 2
        phi_2_dot = 2
    elif time > 4.7 and time < 5.4:
        phi_1_dot = 4.97
        phi_2_dot = -4.97
    elif time >= 5.4 and time <= 7.4:
        phi_1_dot = 2
        phi_2_dot = 2
    elif time > 7.4 and time < 8.1:
        phi_1_dot = 5
        phi_2_dot = -5
    elif time >= 8.1 and time <= 10.2:
        phi_1_dot = 2
        phi_2_dot = 2
    elif time > 10.2 and time < 10.8:
        phi_1_dot = 3.1
        phi_2_dot = -3.1
    elif time >= 10.9:
        phi_1_dot = 0
        phi_2_dot = 0
    
    # Circle
#    if time < 19:
#       phi_1_dot = 3
#       phi_2_dot = 1
 
    # Arrow
#    if time <=2:
#        phi_1_dot = 0.5
#        phi_2_dot = 0.5
#    elif time > 2 and time <= 2.6:
#        phi_1_dot = 5.64
#        phi_2_dot = -5.64
#    elif time > 2.6 and time <= 4.7:
#        phi_1_dot = 2
#        phi_2_dot = 2
#    elif time > 4.7 and time < 5.4:
#        phi_1_dot = -6.8
#        phi_2_dot = 6.8
#    elif time >= 5.4 and time <= 7.4:
#        phi_1_dot = 0.7
#        phi_2_dot = 0.7
#    elif time > 7.4 and time < 8.1:
#        phi_1_dot = 7.3
#        phi_2_dot = -7.3
#    elif time >= 8.1 and time <= 10.2:
#        phi_1_dot = 5
#        phi_2_dot = 5
#    elif time > 10.2 and time < 10.9:
#        phi_1_dot = 8.65
#        phi_2_dot = -8.65
#    elif time >= 10.9 and time <= 13:
#        phi_1_dot = 5
#        phi_2_dot = 5
#    elif time > 13 and time < 13.7:
#        phi_1_dot = 7.63
#        phi_2_dot = -7.63
#    elif time >= 13.7 and time <= 15.8:
#        phi_1_dot = 0.74
#        phi_2_dot = 0.74
#    elif time > 15.8 and time < 16.5:
#        phi_1_dot = -7.11
#        phi_2_dot = 7.11
#    elif time >= 16.5 and time <= 18.6:
#        phi_1_dot = 2
#        phi_2_dot = 2
    return phi_1_dot, phi_2_dot

if __name__ == "__main__":
    try:
        rospy.init_node('kinematics')
        rate = rospy.Rate(100)

        while not rospy.is_shutdown():
          pub = rospy.Publisher("cmd_vel", Twist, queue_size=10)
          twist = Twist()
          i = 0
          theta = 0
          th = []
          phi_1_dot = 0
          phi_2_dot = 0
          x_dot = 0
          y_dot = 0
          theta_dot = 0
          
          for i in range(191):
            phi_1_dot, phi_2_dot = wheel(i/10.0)
            x_dot, y_dot, theta_dot = kine(phi_1_dot, phi_2_dot, theta)
#            s = lambda theta_dot,tt: theta_dot*tt
#            theta,err = integrate.quad(s, i, i+1, args=(theta_dot))
#            s = lambda tt,thd: thd*tt
#            theta,err = integrate.quad(s, i/10.0, (i+1)/10.0, args=(theta_dot))
            theta = theta + theta_dot * (0.1)
#            if theta != theta + theta_dot * (0.1):
#             th[] = theta
            print theta, theta_dot
               
            if abs(x_dot) > abs(y_dot):
             twist.linear.x = x_dot
            elif abs(x_dot) < abs(y_dot):
             twist.linear.x = y_dot
            elif abs(x_dot) == abs(y_dot):
             twist.linear.x = 0

            twist.angular.z = theta_dot
            pub.publish(twist)
            i = i + 1
            time.sleep(0.1)
    
    except rospy.ROSInterruptException:
        pass
