#!/usr/bin/env python

import rospy 
import math
import time
import scipy.integrate as integrate
from std_msgs.msg import String  
from geometry_msgs.msg import Twist

if __name__ == "__main__":
    try:
        rospy.init_node('kinematics')
        rate = rospy.Rate(100)

        while not rospy.is_shutdown():
          pub = rospy.Publisher("cmd_vel", Twist, queue_size=10)
          twist = Twist()
          i = 0
          r = 0.1 # wheel radius
          l = 0.215 # wheel distance to the centerline
          phi_1_dot = 0
          phi_2_dot = 0
          
         # left = 2
         # right = 1
          
          for i in range(191):
            t1 = i/10.0
            
            # Square
#            if t1 <= 2:
#             twist.linear.x = 2*r/2 + 2*r/2
#             twist.angular.z = 0
#            elif t1 > 2 and t1 <= 2.6:
#             twist.linear.x = 0
#             twist.angular.z = (5.64*r)/(2*l)-(r*-5.64)/(2*l)
#            elif t1 > 2.6 and t1 <= 4.7:
#             phi_1_dot = 2
#             phi_2_dot = 2
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 4.7 and t1 < 5.4:
#             phi_1_dot = 4.97
#             phi_2_dot = -4.97
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 5.4 and t1 <= 7.4:
#             phi_1_dot = 2
#             phi_2_dot = 2
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 7.4 and t1 < 8.1:
#             phi_1_dot = 5
#             phi_2_dot = -5
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 8.1 and t1 <= 10.2:
#             phi_1_dot = 2
#             phi_2_dot = 2
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 10.2 and t1 < 10.8:
#             phi_1_dot = 3.1
#             phi_2_dot = -3.1
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 10.9:
#             phi_1_dot = 0
#             phi_2_dot = 0
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
             
            # Circle 
            if t1 < 19:
             phi_1_dot = 3
             phi_2_dot = 1
             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l) 
            elif t1 >= 19:
             twist.linear.x = (r*0)/2 + (r*0)/2
             twist.angular.z = (r*0)/(2*l) + (-r*0)/(2*l) 
             
            # Arrow
#            if t1 <=2:
#             phi_1_dot = 0.5
#             phi_2_dot = 0.5
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 2 and t1 <= 2.6:
#             phi_1_dot = 5.64
#             phi_2_dot = -5.64
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 2.6 and t1 <= 4.7:
#             phi_1_dot = 2
#             phi_2_dot = 2
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 4.7 and t1 < 5.4:
#             phi_1_dot = -6.8
#             phi_2_dot = 6.8
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 5.4 and t1 <= 7.4:
#             phi_1_dot = 0.7
#             phi_2_dot = 0.7
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 7.4 and t1 < 8.1:
#             phi_1_dot = 7.3
#             phi_2_dot = -7.3
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 8.1 and t1 <= 10.2:
#             phi_1_dot = 5
#             phi_2_dot = 5
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 10.2 and t1 < 10.9:
#             phi_1_dot = 8.65
#             phi_2_dot = -8.65
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 10.9 and t1 <= 13:
#             phi_1_dot = 5
#             phi_2_dot = 5
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 13 and t1 < 13.7:
#             phi_1_dot = 7.63
#             phi_2_dot = -7.63
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 13.7 and t1 <= 15.8:
#             phi_1_dot = 0.74
#             phi_2_dot = 0.74
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 > 15.8 and t1 < 16.5:
#             phi_1_dot = -7.11
#             phi_2_dot = 7.11
#             twist.linear.x = (r*phi_1_dot)/2 + (r*phi_2_dot)/2
#             twist.angular.z = (r*phi_1_dot)/(2*l) + (-r*phi_2_dot)/(2*l)
#            elif t1 >= 16.5 and t1 <= 18.6:
#             twist.linear.x = (r*2)/2 + (r*2)/2
#             twist.angular.z = (r*2)/(2*l) + (-r*2)/(2*l)
#            elif t1 > 18.6:
#             twist.linear.x = (r*0)/2 + (r*0)/2
#             twist.angular.z = (r*0)/(2*l) + (-r*0)/(2*l)
        
            pub.publish(twist)
            i = i + 1
            time.sleep(0.1)
    
    except rospy.ROSInterruptException:
        pass
